import React, {useState} from 'react'
import './App.css'
import './styles/button.css'
import Header from './components/Header'
import PriceList from './components/PriceList'
import Modal from './components/Modal'
import Card from './components/Card'
import Payment from './components/Payment'
import SocialList from './components/SocialList'
import {PRICE_LIST_CONFIG, SOCIAL_LIST} from "./constants"

function App() {
    const [modalOpen, setModalOpen] = useState(false);

    function onToggle() {
        setModalOpen(!modalOpen);
    }

    return (
        <div className="app">
            <Header onToggle={onToggle} count={8}></Header>
            <PriceList priceLists={PRICE_LIST_CONFIG}></PriceList>
            <Modal active={modalOpen} onCloseClick={() => setModalOpen(false)}>
                <Card content={<Payment />} footer={<SocialList items={SOCIAL_LIST} title="Более 20 популярных способов оплаты"/>}></Card>
            </Modal>
        </div>
    );
}

export default App;
