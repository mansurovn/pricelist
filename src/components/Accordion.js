import React, {useState} from 'react'
import PropTypes from 'prop-types'

function Accordion(props) {
    function onToggle(state) {

    }

    return (
        <div className={`accordion${props.className ? ' ' + props.className : ''}`}>
            {
                props.children.map((child, index) => {
                    if (props.defaultItemOpened !== undefined && index === props.defaultItemOpened) {
                        child.props.opened = true;
                    }

                    child.props.onToggle = onToggle;
                })
            }
        </div>
    )
}

Accordion.propTypes = {
    defaultItemOpened: PropTypes.number
};

export default Accordion;