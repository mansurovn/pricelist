import React, {useState} from 'react'
import {ReactComponent as AngleDown} from '../icons/angle-down.svg'

import '../styles/accordion.css'

export default function AccordionItem(props) {
    const [opened, setOpened] = useState(false);

    return (
        <div className={`accordion-item${opened ? ' accordion-item_opened' : ''}`}>
            <button type="button" className="accordion-toggle" onClick={() => setOpened(!opened)}>
                {props.toggle ? props.toggle : null}
                <span className="accordion-toggle__icon">
                    <AngleDown/>
                </span>
            </button>
            <div className="accordion-hidden">
                {props.hidden ? props.hidden : null}
            </div>
        </div>
    )
}