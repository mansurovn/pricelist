import React from 'react'
import PropTypes from 'prop-types'

function Button(props) {
    return (
        <button className={`button button_${props.kind}${props.classname ? ' ' + props.classname : ""}`} type={props.type}>
            {props.icon ? (<img src={props.icon.src} alt={props.icon.alt || props.text} className={`button__icon${props.iconClassname ? ' ' + props.iconClassname : ""}`}/>) : null}
            {props.text}
        </button>
    )
}

Button.defaultProps = {
    kind: 'filled',
    type: 'button',
};

Button.propTypes = {
    kind: PropTypes.string,
    type: PropTypes.string,
    classname: PropTypes.string,
    iconClassname: PropTypes.string,
    icon: PropTypes.shape({
        src: PropTypes.string,
        alt: PropTypes.string
    }),
    text: PropTypes.string.isRequired
};

export default Button;