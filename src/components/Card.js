import React from 'react'

import '../styles/card.css'

export default function Card(props) {
    return (
        <div className="card">
            <div className="card-content">
                {props.content}
            </div>
            <div className="card-footer">
                {props.footer}
            </div>
        </div>
    )
}