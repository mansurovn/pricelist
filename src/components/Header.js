import React from 'react'
import PropTypes from 'prop-types'
import { ReactComponent as ArrowLeft } from '../icons/arrow-left.svg'

import '../styles/header.css'

function Header (props) {
    return (
        <header className="header">
            <div className="header-container">
                <button className="header__back-btn" type="button">
                    <ArrowLeft />
                    Прайс-лист
                </button>
                <button type="button" className="header-price-toggle"
                        onClick={props.onToggle}>
                        <span className="header-price-toggle-inner">
                            <span className="header-price-toggle__bar"></span>
                        </span>
                    {props.count ? (
                        <span className="header-price-toggle__label">{props.count}</span>
                    ) : null}
                </button>
            </div>
        </header>
    )
}

Header.propTypes = {
    count: PropTypes.number,
    onToggle: PropTypes.func
}

export default Header;