import React from 'react'
import PropTypes from 'prop-types'
import {CSSTransition} from 'react-transition-group'
import {ReactComponent as Close} from '../icons/close.svg'

import '../styles/modal.css'
import '../styles/transitions.css'

function Modal (props) {
    return (
        <CSSTransition in={props.active} timeout={400} classNames="fade">
            <div className={`modal${props.active ? ' modal_active' : ''}`}>
                <div className="modal-overlay" onClick={props.onCloseClick}></div>
                <div className="modal-inner">
                    {props.children}
                    <button className="modal__close" type="button" onClick={props.onCloseClick}><Close/></button>
                </div>
            </div>
        </CSSTransition>
    )
}

Modal.propsDefault = {
    active: false
};

Modal.propsType = {
    active: PropTypes.bool,
    onCloseClick: PropTypes.func
};

export default Modal;