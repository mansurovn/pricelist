import React from 'react'
import Button from './Button'

import '../styles/payment.css'

export default function Payment(props) {
    return (
        <div className="payment">
            <div className="payment__title">Пополнить баланс</div>
            <form className="payment-form">
                <div className="payment-form-group">
                    <div className="payment-form__label">Укажите сумму</div>
                    <input type="text" placeholder="Не менее 100 ₽" className="payment-form__control"/>
                </div>
                <div className="payment-form-buttons">
                    <div className="payment-form-card-container">
                        <Button kind="filled"
                                classname="payment-form__card"
                                iconClassname="payment-form__card-icon"
                                text="Пополнить с карты"
                                icon={{src: '/images/cards.svg', alt: 'Платежные системы'}}
                                type="submit"/>
                    </div>
                    <div className="payment-form-or">
                        или
                    </div>
                    <div className="payment-form-services">
                        <Button kind="linear"
                                classname="payment-form__qiwi payment-form__btn"
                                iconClassname="payment-form__qiwi-icon payment-form__btn-icon"
                                text="Qiwi"
                                icon={{src: '/images/qiwi.svg', alt: 'qiwi'}}
                                type="submit"/>
                        <Button kind="linear"
                                classname="payment-form__pp payment-form__btn"
                                iconClassname="payment-form__pp-icon payment-form__btn-icon"
                                text="PayPal"
                                icon={{src: '/images/PayPal.svg', alt: 'PayPal'}}
                                type="submit"/>
                    </div>
                    <div className="payment-form-other-container">
                        <Button kind="linear"
                                classname="payment-form__other"
                                text="Оплатить другим способом"
                                type="submit"/>
                    </div>
                </div>
            </form>
        </div>
    )
}