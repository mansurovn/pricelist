import React, {Component} from 'react'
import PropTypes from 'prop-types'
import AccordionItem from './AccordionItem'
import ServiceCard from './ServiceCard'

import '../styles/price-list.css'

export default class PriceList extends Component {

    static defaultProps = {
        priceLists: []
    }

    static propTypes = {
        priceLists: PropTypes.arrayOf(PropTypes.shape({
            icon: PropTypes.string,
            title: PropTypes.string.isRequired,
            note: PropTypes.string,
            content: PropTypes.any
        }))
    }

    renderAccordionItems() {
        return this.props.priceLists.map((priceList, index) => (
            <AccordionItem className="accordion-item"
                           toggle={this.renderAccordionToggle(priceList.icon, priceList.title)}
                           hidden={this.renderAccordionHidden(priceList.content, priceList.note)}
                           key={index}>
            </AccordionItem>
        ));
    }

    renderAccordionToggle(icon, title) {
        return (
            <span className="price-list-toggle">
                    {icon ? (
                        <img src={`/images/${icon}.svg`} alt={icon} className="price-list-toggle__icon"/>
                    ) : null}
                <span className="price-list-toggle__text">{title}</span>
            </span>
        );
    }

    renderAccordionHidden(content, note) {
        return (
            <div>
                {content && content.length ? (
                    content.map((contentItem, index) => (
                        <ServiceCard {...contentItem} key={index}></ServiceCard>
                    ))
                ) : null}
                {note ? (
                    <div className="price-list__note">
                        {note}
                    </div>
                ) : null}
            </div>
        )
    }

    render() {
        return (
            <section className="price-list">
                <div className="container price-list-container">
                    <div className="price-list-content">
                        <div className="price-list-title">
                            <h1 className="price-list-title__value">Прайс-лист</h1>
                        </div>
                        <div className="price-list-accordion">
                            {this.renderAccordionItems()}
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
