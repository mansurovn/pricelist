import React from 'react'

import '../styles/services-card.css'
import '../styles/features.css'

export default props => {
    function renderFeatures(feature, index) {
        return (
            <li className="features-item" key={index}>
                <div className="feature-item__title">{feature.title}</div>
                <div className="feature-item__value">{feature.value}</div>
            </li>
        )
    }

    return(
        <div className="service-card">
            <div className="service-card-top">
                {props.icon ? (
                    <img src={`/images/${props.icon}.png`} alt={props.icon} className="service-card__icon" />
                ) : null}
                <div className="service-card__title">{props.title}</div>
            </div>
            <div className="service-card__desc">{props.desc}</div>
            {props.features && props.features.length ? (
                <ul className="service-card-features">
                    {props.features.map((feature, index) => renderFeatures(feature, index))}
                </ul>
            ) : null}
        </div>
    );
}