import React from 'react'
import PropTypes from 'prop-types'

import '../styles/social-list.css'

function SocialList(props) {
    function renderItems(item, index) {
        return (
            <a href={item.link} key={index} className="social-list__link">
                <img src={`/images/${item.icon}.svg`} alt={item.icon} className="social-list__icon"/>
            </a>
        );
    }

    return (
        <div className="social-list">
            {props.title ? (
                <div className="social-list__title">{props.title}</div>
            ) : null}
            <div className="social-list-items">
                {props.items.map(renderItems)}
            </div>
        </div>
    )
}

SocialList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        link: PropTypes.string.isRequired
    })),
    title: PropTypes.string
};

export default SocialList;